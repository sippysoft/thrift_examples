#!/usr/bin/env perl

use strict;
use thrift_stub_dir::Types;
use Thrift::BufferedTransport;
use Thrift::BinaryProtocol;

package FileTransport;
use base ("IO::File");

sub new {
    my $classname   = shift;
    my $self = new IO::File(@_) or return undef;
    return bless($self,$classname);
}

sub readAll {
    my $self = shift;
    my $len = shift;
    $self->read(my $buf, $len);
    return $buf;
}

package main;

my $fname = "../calls-thrift.bin";

my $fd = new FileTransport($fname) or die "cannot open $fname: $!";
my $t = new Thrift::BufferedTransport($fd);
my $p = new Thrift::BinaryProtocol($t);
while (!$fd->eof) {
    my $obj = new Calls();
    $obj->read($p);
    print "i_call = ".$obj->i_call.", cli = ".$obj->cli.", cld = ".$obj->cld."\n";
}

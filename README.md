The Sippy Softswitch can be configured to export CDR's in [Thrift][1]
encoded format. This feature is useful for VoIP operators who need fast
access to CDR data from there Sippy Softswitch. Typically, operators use
this facility to efficiently ingest Sippy CDR data in to an external data
warehouse.

This repository offers a quick introduction, sample Thrift encoded CDR's,
and a python script to help you get started with Thrift encoded Sippy CDR
files.

# Thrift schema file

The Thrift schema files for your Sippy Softswitch version are always
available on your Sippy Softswitch's filesystem:

    /usr/local/share/ssp/

The schema is typically changes when the Sippy Softswitch is updated to
a new version. It's good practice to regenerate your generated Thrift
stubs after every Sippy upgrade.

# Ready made Python example

In this directory you can find a sample exported data file
`calls-thrift.bin` and python script `read_calls.py` which reads the data
file and prints the `i_call`, `CLI` and `CLD` values to standard out on
your console. This data was generated from a v4.4 Softswitch and is for
learning purposes only.

To run this example, you need to install the python thrift library, which
can typically be achieved using the `pip install thrift` command.

# Creating the stub code for Python

When building a real integration, you will want to generate your own
Thrift code to decode Sippy CDR's. Thrift can generate stubs in Java, C,
go, JavaScript and many more. The following example shows you how to
generate python stubs. The following instructions assume you are using
some flavour of Unix.

Copy the following files from your Sippy Softswitch to your a local
directory

- `/usr/local/share/ssp/types.thrift`
- `/usr/local/share/ssp/call_details.thrift`

Join the two files into a single file called `ssp.thrift`

    $ cat types.thrift call_details.thrift > ssp.thrift

Create a new directory for your module

    $ mkdir thrift_stub_dir 

Use the thrift command to generate your thrift stubs

    $ thrift -strict -gen py:new_style -out thrift_stub_dir ssp.thrift

After this you will have the stub code in the `thrift_stub_dir`:

	$ tree thrift_stub_dir
	thrift_stub_dir
	├── __init__.py
	└── ssp
		├── constants.py
		├── __init__.py
		└── ttypes.py

	1 directory, 4 files


You can now import this generated module to your python code.

  [1]: https://thrift.apache.org/

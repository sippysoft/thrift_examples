#!/usr/bin/env python2

from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol

import sys

sys.path.append("./thrift_stub_dir")
from ssp import ttypes


fd = file("calls-thrift.bin")
t = TTransport.TFileObjectTransport(fd)
p = TBinaryProtocol.TBinaryProtocolAccelerated(t)
while True:
    obj = ttypes.Calls()
    try:
        obj.read(p)
        print("i_call = %s, cli = %s, cld = %s" % (obj.i_call, obj.cli, obj.cld))
    except EOFError:
        break
